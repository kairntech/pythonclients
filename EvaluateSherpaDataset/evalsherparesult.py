# -*- coding: utf-8 -*-
"""
Stefan / Kairntech, Sept 2020

This script allows to to compare/evaluate a dataset(json) exported from sherpa that contains the manual 
annotations with a second json that results from sending via the API the same corpus to Sherpa to get 
the automatic annotations (created with anotatesherpadataset.py)

You then call this script with the two json files and a file name where the result (TAB separated table)
will be writte to. 

You will get the list of entities and the respective numbers for recall, precision and how many times
the entity has been found correctly, how many false positives and negatives and examples for these cases. 

Attention / TODO: 
- the script requires identical spans! partial extraction will count as an error    
- there may be counting errors when an entity is found several times in a segment
- recall an Precision for the Types and overall not written into the Excel list, only to STDOUT
- I send the complete corpus, but document have been used to train the model in Sherpa... so the
  results will by systematically too high.

Still: the resulting Excel table allows (after "format as table") to quickly focus on entities
that are often not found or that are often not found, etc ...      
"""

import sys
import json
import argparse
from   collections import defaultdict

parser = argparse.ArgumentParser(description="Evaluate a sherpa result against the manually annotated corpus")

parser.add_argument("-m", "--manual-corpus",  help="dataset (json) exported from sherpa")
parser.add_argument("-s", "--sherpa-result",  help="dataset (json) processed by sherpa")
parser.add_argument("-o", "--output",         help="filepath for output (tab separated csv file)")

args   = parser.parse_args()

if args.manual_corpus is None or args.sherpa_result is None:
    parser.print_help()
    exit()

manualdocs    = defaultdict(dict)
automaticdocs = defaultdict(dict)

with open(args.manual_corpus) as json_file:
    _manualdocs = json.load(json_file)
    for doc in _manualdocs:
        ident = doc['identifier']
        manualdocs[ident] = doc
        
with open(args.sherpa_result) as json_file:
    _automaticdocs = json.load(json_file)
    for doc in _automaticdocs:
        ident = doc['identifier']
        automaticdocs[ident] = doc
            
# sanity check
num_manual = 0
for ident in manualdocs:
    num_manual += 1
    if ident not in automaticdocs:
        print("document %s not found in automatic corpus" % ident)
num_automatic = 0
for ident in automaticdocs:
    num_automatic += 1
    if ident not in manualdocs:
        print("document %s not found in manual corpus" % ident)
print("we have %s documents in the manual corpus and %s in the automatic corpus" % (num_manual, num_automatic))

# Bookkeeping
Correct       = 0
FalseNegative = 0
FalsePositive = 0
CorrectEntity       = defaultdict(int)
FalseNegativeEntity = defaultdict(int)
FalsePositiveEntity = defaultdict(int) 
CorrectType         = defaultdict(int)
FalseNegativeType   = defaultdict(int)
FalsePositiveType   = defaultdict(int) 
All = 0 
AllEntity           = defaultdict(int)
AllType             = defaultdict(int)

FalseNegativeSegment= defaultdict(dict)
FalsePositiveSegment= defaultdict(dict)

# now first search for missed entities
for ident in manualdocs:
    
        docname = manualdocs[ident]['documentIdentifier']
        segtext = manualdocs[ident]['text'].replace('\n', ' ').replace("\t", " ")
        
        manualdoc = manualdocs[ident]
        manualannotations = manualdoc['annotations']
        
        automaticdoc = automaticdocs[ident]
        automaticannotations = automaticdoc['annotations']
        
        manualentities    = [(anno['text'],anno['labelName']) for anno in manualannotations]
        automaticentities = [(anno['text'],anno['labelName']) for anno in automaticannotations]
        
        for me in manualentities:
            meE = me[0].replace('\n', ' ').replace("\t", " ")
            meT = me[1]
            All += 1
            AllType[meT] += 1
            AllEntity[meE] += 1
            
            if me not in automaticentities:
                FalseNegative += 1
                FalseNegativeEntity[meE] += 1
                FalseNegativeType[meT] += 1     
                FalseNegativeSegment[meE][segtext] = 1
                #print("Entity %s is missing in doc %s" % (meE.encode('ascii','ignore'), docname.encode('ascii','ignore')))
            else:
                Correct += 1
                CorrectEntity[meE] += 1
                CorrectType[meT] += 1
                #print("Entity %s was found in doc %s" % (meE.encode('ascii','ignore'), docname.encode('ascii','ignore')))
                
        for me in automaticentities:
            if me not in manualentities:
                FalsePositive += 1
                FalsePositiveEntity[meE] += 1
                FalsePositiveType[meT] += 1
                FalsePositiveSegment[meE][segtext] = 1
                #print("Entity %s found wrongly in doc %s" % (meE.encode('ascii','ignore'), docname.encode('ascii','ignore')))

Recall = Correct / (Correct + FalseNegative)
Precision = Correct / (Correct + FalsePositive)

print("Recall = %.2f" % Recall)
print("Precision = %.2f" % Precision)

for t in AllType: 
    r = 0
    p = 0
    c = CorrectType[t]
    m = FalseNegativeType[t]
    f = FalsePositiveType[t]
    if c+m > 0:
        r = c/(c+m)
    if c+f > 0:
        p = c/(c+f)
    print("Type: %s, recall = %.2f, precision = %.2f" % (t, r, p))
    
with open(args.output, "w") as out:
    out.write("ENTITY\tRECALL\tPRECISION\tCORRECT\tFALSE\tMISSED\tEXAMPLE FOR MISSED\tEXAMPLE FOR FALSE\n")    

    for t in AllEntity: 
        r = 0
        p = 0
        c = CorrectEntity[t]
        m = FalseNegativeEntity[t]
        f = FalsePositiveEntity[t]
        if c+m > 0:
            r = c/(c+m)
        if c+f > 0:
            p = c/(c+f)
        #print("Entity: %s, recall = %.2f, precision = %.2f (c:%s,m:%s,f:%s)" % (t, r, p, c, m, f))
        out.write("%s \t%.2f \t%.2f \t%s \t%s \t%s \t" % (t,r,p,c,f,m))
        for seg in FalseNegativeSegment[t]:
            out.write("<<%s>>" % seg)
        out.write("\t")
        for seg in FalsePositiveSegment[t]:
            out.write("<<%s>>" % seg)
        out.write("\n")
    
    out.close()
