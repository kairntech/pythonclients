

Experiment to evaluate a sherpa model,
Stefan, Sept 2020

- Export a manually annotated (with Entities) dataset from Sherpa as a json file

- call the annotatesherpadataset.py to send this json file to sherpa
in order to get the annotations according to the training model
(calling the script without arguments will return a help text).

- then you have two json files,
  - the exported dataset with the manualy annotations
  - the results of calling the trained model on the same documents

- evalsherparesults.py will then compare the two corpora and create a
table overview about the entities that are found correctly, missed and
falsely found as well as the segment texts where this occurred.

Opening the resulting table and "format as a table" allows then to
sort by how often it was missed, or how often it was falsely assigned
etc etc,...

This allows users to get an idea what the model does and what not
(yet). Might make sense to consider evaluation options like that for
future Sherpa versions...

Enjoy,
Stefan
