# -*- coding: utf-8 -*-
'''
Sample script: Sending documents to a sherpa annotation model
Stefan Geißler, March 2020
This is NOT Kairntech software! This script is an example how a client 
using the Kairntech API might look like. No warranty!
 
'''
import sys
import requests
import json
import argparse
import glob
import logging
import pandas as pd
from   collections import defaultdict
import http.client as http_client
from   sherpaapi_helper import * 

parser = argparse.ArgumentParser(description="Interact with the Sherpa REST service")

parser.add_argument("-S", "--server",         help="IP address and port of the Sherpa service", default="https://sherpa.kairntech.com")
parser.add_argument("-u", "--user",           help="your sherpa username", default="YOUR-USERNAME-HERE")
parser.add_argument("-p", "--password",       help="your sherpa password", default="YOUR-PASSWORD-HERE")
parser.add_argument("-m", "--model",          help="Name of the model to be called")
parser.add_argument("-a", "--annotator",      help="Name of the annotator to be called")
parser.add_argument("-t", "--text-document",  help="txt document to process")
parser.add_argument("-d", "--text-directory", help="or directory of txt documents to process")
parser.add_argument("-o", "--output",         help="filepath for output (tab separated csv file)")
parser.add_argument("-x", "--maxtextlength",  help="chunk text into pieces of n chars", default=1000)
parser.add_argument("-l", "--logginglevel",   help="logginglevel: DEBUG, INFO, WARNING, ERROR or CRITICAL",
                    default='INFO')
args         = parser.parse_args()

logginglevel = logging.getLevelName(args.logginglevel)
logging.basicConfig(level=logginglevel)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logginglevel)
requests_log.propagate = True

maxtextlength = int(args.maxtextlength)
server        = args.server + '/api'
login_info    = json.dumps({"email": args.user, 
                         "password": args.password})
filecnt       = 0 
# authenticate
mytoken =  get_token(server, login_info)
if mytoken is None:
    print("Could not log in to server '%s' using user '%s' and password '%s'" % (server,args.user,args.password))
    parser.print_help()
    exit()

# if no model is specified, show the list of existing models
if args.model is None:
    print("No project specified!") 
    get_projects(server, mytoken)
    parser.print_help()
    exit()

# if no annotator is specified, show the list of existing annotators
if args.annotator is None:
    print("No annotator specified!")
    get_annotators(server, args.model, mytoken)
    parser.print_help()
    exit()
    
output = pd.DataFrame()

if args.text_document is not None:
    text = open(args.text_document, "r",encoding='UTF-8', errors='ignore').read()
    result = call_sherpa(text,args.model,args.annotator,server,mytoken,maxtextlength)
    result['document'] = args.text_document
    output = output.append(result, ignore_index=True)
elif args.text_directory is not None:
    txtfiles = glob.glob("%s/*txt" % args.text_directory)
    jsonfiles = glob.glob("%s/*json" % args.text_directory)
    files = txtfiles + jsonfiles
    numfiles = len(files)
    for file in files:
        filecnt += 1
        print("%s/%s: processing file %s ..." % (filecnt,numfiles,file))
        text = open(file, "r",encoding='UTF-8', errors='ignore').read()
        result = call_sherpa(text,args.model,args.annotator,server,mytoken,maxtextlength)
        result['document'] = file
        output = output.append(result, ignore_index=True)
else:
    print("error: you need to provide input either as a text file (-t) or as a directory of text files (-d)")
    parser.print_help()
    exit()

output = output.set_index('document')

print(output)

if args.output is not None: 
    output.to_csv(args.output, sep='\t')
    print("output written to %s" % args.output)
    
