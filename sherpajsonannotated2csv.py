# -*- coding: utf-8 -*-
'''
Sample script: Sending documents to a sherpa annotation model
Stefan Geißler, March 2020
This is NOT Kairntech software! This script is an example how a client 
using the Kairntech API might look like. No warranty!
 
'''
import sys
import json
import argparse
import glob
import logging
import pandas as pd
from   collections import defaultdict

parser = argparse.ArgumentParser(description="Interact with the Sherpa REST service")

parser.add_argument("-t", "--text-document",  help="txt document to process")
parser.add_argument("-d", "--text-directory", help="or directory of txt documents to process")
parser.add_argument("-o", "--output",         help="filepath for output (tab separated csv file)")

args         = parser.parse_args()

output = pd.DataFrame()

def json2annotations(jsontext):

    if isinstance(jsontext,list):
        jsontext = jsontext[0]
    
    results = {}
    try:
        annotations = jsontext['annotations']
        documenttext = jsontext['text']
        
        for annotation in annotations:
            if 'label' in annotation:
                ln = 'label'
            else:
                ln = 'labelName' 
            start   = annotation['start']
            end     = annotation['end']
            term    = documenttext[start:end]
            term    = term.replace("\n", " ")
            type    = annotation[ln]
            if type in results:
                results[type].extend([term])
            else:
                results[type] = [term]
        #print("before results = %s" % results)
        for key in results.keys():
            list_set = set(results[key])
            list_string = ";".join(list_set)
            results[key] = list_string
            #print("after results = %s" % results)
    except Exception as e:
        print("error parsing json: %s" % e)
    return results



if args.text_document is not None:
    text = open(args.text_document, "r",encoding='UTF-8', errors='ignore').read()
    jsontext = json.loads(text)
    result = json2annotations(jsontext)
    result['document'] = args.text_document
    output = output.append(result, ignore_index=True)
elif args.text_directory is not None:
    jsonfiles = glob.glob("%s/*json" % args.text_directory)
    numfiles = len(jsonfiles)
    filecnt = 0
    for myfile in jsonfiles:
        filecnt += 1
        print("%s/%s: processing file %s ..." % (filecnt,numfiles,myfile))
        text = open(myfile, "r",encoding='UTF-8', errors='ignore').read()
        jsontext = json.loads(text)
        print("type(jsontext) = %s" % type(jsontext))
        print("jsontext = %s" % jsontext)
        if len(jsontext) > 0:
            result = json2annotations(jsontext)
            result['document'] = myfile
            output = output.append(result, ignore_index=True)
else:
    print("error: you need to provide input either as a text file (-t) or as a directory of text files (-d)")
    parser.print_help()
    exit()

output = output.set_index('document')

print(output)

if args.output is not None: 
    output.to_csv(args.output, sep='\t')
    print("output written to %s" % args.output)
    
