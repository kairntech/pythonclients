##
## go through categorized txt corpus, compute "fingerprint" for each category
##  and then
##  - rank each txt according to how 'close' it is to its category
##    (the ones that are distant may be outliers/errors?)
##  - rank each category according to how close it is to each other category
##    (maybe there are categories that could be merged because they are quite similar anyway?)

import numpy as np
import sys, math
from tensorflow.keras.utils import to_categorical
from sklearn.datasets import load_files
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import pickle
from collections import defaultdict
from scipy import spatial

def get_items(s):
    s_coo = s.tocoo()
    return set(zip(s_coo.row, s_coo.col))

if len(sys.argv) < 2:
    print("usage: %s corpusdir" % sys.argv[0])
    exit()

    # length of the input vector
#MAX_FEATS = 100000
MAX_FEATS       = 10000
EPOCHS          = 30
DROPOUT         = 0.2
INPUTLENGTH     = 500
corpusdir       = sys.argv[1]
USEEMBEDDING    = False

print("loading corpus from %s ... " % corpusdir)
corpus = load_files(corpusdir, encoding='utf8', decode_error='replace')
print("corpus target names = %s" % corpus.target_names)


# don't need test corpora here 
#X_train, X_test, y_train, y_test = train_test_split(corpus.data, corpus.target, test_size=0.0)

X_train = corpus.data
y_train = corpus.target

print("y_train = %s" % y_train)
# compute the counts
y_train_counts = defaultdict(int)
for y in y_train:
    y_train_counts[y] += 1

print("y_train_counts: %s" % y_train_counts)

firstX = X_train[0]
firstY = y_train[0]
print("firstX = %s" % firstX[:200])
print("firstY = %s" % firstY)

print("training data : %s" % len(X_train))

# compute class weights to better handle imbalanced data sets
class_weights = class_weight.compute_class_weight('balanced',
                                                   np.unique(y_train),
                                                   y_train)

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)
print("training data shape (raw): %s" % str(X_train_counts.shape))

# now let's only keep the top MAX_FEATS words ...
print("computing counts ...")
count_vect     = CountVectorizer(max_features=MAX_FEATS)
X_train_counts = count_vect.fit_transform(X_train)
print("training data shape (counts): %s" % str(X_train_counts.shape))


print("computing tf*idf ...")
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
print("training data shape (tfidf): %s" % str(X_train_tfidf.shape))

y_train         = to_categorical(y_train)
catfingerprints = defaultdict()
num4cat         = defaultdict(int)


#print("X_train_tfidf[0] = %s" % X_train_tfidf[0])
#print("type(X_train_tfidf) = %s" % type(X_train_tfidf))

X_train_vecs = []
allitems = get_items(X_train_tfidf)

for num in range(len(X_train)):
    thisvec = X_train_tfidf[num]
    #print("thisvec = %s" % thisvec)
    newvec = np.zeros(MAX_FEATS)
    for pos in range(MAX_FEATS):
        if (num,pos) in allitems: 
            newvec[pos] = X_train_tfidf[num,pos]
    #print("%s: newvec = %s" % (num,newvec))
    X_train_vecs.append(newvec)
    
all = zip(corpus.target, X_train_vecs)
for (cat,vec) in all:
    catname = corpus.target_names[cat]

    if catname not in catfingerprints:
        catfingerprints[catname] = np.array(vec)
    else:
        catfingerprints[catname] += np.array(vec)
    num4cat[catname] += 1

for cat in corpus.target_names:
    vec = catfingerprints[cat]
    num = num4cat[cat]
    #print("cat: %s, vec: %s" % (cat,vec))
    catfingerprints[cat] = vec / num

comparefingerprints = defaultdict()
for cat1 in catfingerprints:
    for cat2 in catfingerprints:
        if cat1 > cat2:
            continue
        f1 = catfingerprints[cat1]
        f2 = catfingerprints[cat2]
        cos = 1 - spatial.distance.cosine(f1, f2)
        comparefingerprints["%s / %s" % (cat1, cat2)] = cos

sorted_x = sorted(comparefingerprints.items(), key=lambda kv: kv[1])        
for pair in sorted_x:
    print("categories %s, similarity: %s" % ((pair[0]), pair[1]))

    
### now rank which documents don't fint into their resp. categories
outliers = defaultdict(dict)

for num in range(len(X_train)):
    filename = corpus.filenames[num]
    catcode  = corpus.target[num]
    catname  = corpus.target_names[catcode]
    #print("file: %s, cat %s" % (filename, catname))

    filevec = X_train_vecs[num]
    catvec  = catfingerprints[catname]
    #print("filevec = %s" % filevec)
    #print("catvec  = %s" % catvec)
    try:
        cos = 1 - spatial.distance.cosine(filevec, catvec)
    except Exception as e:
        print("error %s, computing cos, filevec = %s, catvec = %s" % (e, filevec, catvec))
        cos = 0.0
        
    #print("%s = cos(%s,%s)" % (cos, filename, catname))
    if math.isnan(cos):
        print("error: cos = nan for %s / %s" % (catname, filename))
        continue
    outliers[catname][filename] = cos

for catname in outliers:
    print("===========================================")
    print("catname = %s" % catname)
    sortedfiles = sorted(outliers[catname].items(), key=lambda kv: kv[1])
    for pair in sortedfiles:
        print("%s cat: %s, file: %s" % (pair[1], catname, pair[0])) 
