'''
calling the PDF conversion & Entity Fishing REST API call 'disambiguate'


'''
import sys
from urllib import request
import requests
import json
import argparse
from nerd import nerd_client
# -*- coding: utf-8 -*-

parser = argparse.ArgumentParser(description="Interact with the Entity Fishing REST service")

parser.add_argument("-S", "--server", default="http://localhost:8090", help="IP address and port of the Entity fishing service")
parser.add_argument("-p", "--pdf-document",  help="PDF document to process")
parser.add_argument("-t", "--text-document", help="txt document to process")
parser.add_argument("-l", "--language",      help="language")
parser.add_argument("-s", "--string",        help="string to process")
parser.add_argument("-e", "--example-input", help="process a sample text", action='store_true')
parser.add_argument("-P", "--add-concept-path", help="adds Wikidata concept path", action='store_true')

args = parser.parse_args()
if args.language == None:
    args.language = 'en'

#print("working with language %s" % args.language)

myNerdClient = nerd_client.NerdClient(apiBase=args.server + '/service')

if myNerdClient is None:
    print("error: cannot connect to Nerd. Make sure you have launched a nerd service.")
    print("   (here: /home/stefan/Tools/nerd: mvn clean jetty:run")
    exit()

text = """Determination of pharmacokinetic parameters of estradiol (E2), estrone
(E1), estrone sulfate (E1-S) and cyproterone acetate (CPA), SHBG, CBG,
protein binding of estradiol and estrone; safety; blood sampling: days 1,
10 and 12 over 24 h, day 21 over the next 32 h, once a day on several days
up to day 36"""

#text = """Des militants prodemocratie hongkongais contestent, jeudi 31 octobre, devant la justice l'inte#rdiction du port du masque decretee par les autorites pour en finir avec la contestation, au moment ou H#alloween devrait servir de pretexte a une nouvelle manifestation.

#L'ex-colonie britannique traverse depuis juin sa pire crise politique depuis sa retrocession a Pekin en #1997 avec des manifestations et actions quasi quotidiennes pour denoncer le recul des libertes et les in#gerences grandissantes de la Chine dans les affaires de la region semi-autonome. Sans chef ni porte-paro#le, cette mobilisation s'organise essentiellement sur les reseaux sociaux, et des appels ont ete lances #sur les forums en ligne pour participer jeudi soir a une nouvelle manifestation, alors que la ville cele#brera Halloween."""

#text = """Try this: Obama and Cheney discover hidden family ties WASHINGTON, Oct 17, 2007 (AFP) Obama and Cheney discover hidden family ties"""


if args.text_document is not None:
    text = open(args.text_document, "r",encoding='UTF-8', errors='ignore').read()
elif args.string is not None: 
    text = args.string
elif args.pdf_document is not None:
    print("extracting from %s, language = %s .." % (args.pdf_document, args.language))
    #response = myNerdClient.disambiguate_pdf(args.pdf_document, "en")
    response = myNerdClient.disambiguate_pdf(args.pdf_document, args.language)
    #print("response = %s" % str(response[0]))
    print(json.dumps(response[0], indent=4))
    exit()
elif args.example_input:
    #text already set above
    pass
else:
    print("error: you need to provide either a string (-s), a text file (-t)")
    print("       a pdf file (-p) or the -e switch to test a sample text")
    print("       (use \"-l de\" to specify another language, 'en' is default)") 
    exit()

# disambiguate_query seems to be meant for shorter texts (=return ALL wikidata concepts)
#returntuple = myNerdClient.disambiguate_query(text)

# disambiguate_text seems to make an nbest choice ...? 
returntuple = myNerdClient.disambiguate_text(text)
jsoncontent = returntuple[0]

def get_paths_to_root(wid, indent=0):
    prefix = "  " * indent
    #print(prefix + "%s: get_paths_to_root(%s)" % (indent, wid))
    concept = myNerdClient.get_concept(wid)
    #print("concept = %s" % str(concept))
    # sometimes a concept doesn't seem to have a prefName? (error?)
    pref = "noPrefName"
    if 'preferredTerm' in concept[0]:
        pref    = concept[0]['preferredTerm']
    # HACK
    if indent > 5:
        #print(prefix + "%s: returning %s" % (indent, pref))
        return [pref]
    
    #print(prefix + "%s: pref = %s" % (indent,pref))
    allupperpaths = []
    if 'statements' not in concept[0]:
        return [pref]
    for statement in concept[0]['statements']:
        if statement['propertyId'] == 'P279':
            upper = statement['value']
            upperpaths = get_paths_to_root(upper, indent + 1)
            for upperpath in upperpaths:
                allupperpaths.append(upperpath)
    #print(prefix + "%s:allupperpaths = %s" % (indent,allupperpaths))
    if len(allupperpaths) > 0:
        #print(prefix + "%s:pref = %s" % (indent,pref))
        allupperpaths = [pref + '/' + path for path in allupperpaths]
    else:
        allupperpaths = [pref]
        #print(prefix + "%s:initialized allupperpath = %s" % (indent,allupperpaths))
    #print(prefix + "%s:returning from get_paths_to_root(%s) with %s" % (indent,wid,allupperpaths))
    return allupperpaths


# if
if args.add_concept_path:
    entities = jsoncontent['entities']
    for entity in entities:
        #print("===============================================")
        name = entity['rawName']
        #print("rawName = %s" % name)
        if 'wikidataId' in entity.keys():
            wid  = entity['wikidataId']
            paths = get_paths_to_root(wid)
            paths = [name + '/' + path for path in paths] 
            #print("pathstoroot(%s) = " % (wid))
            #for p in paths:
                #print("   %s" % p)
            entity['wikidataTaxonomy'] = paths




print("%s" % json.dumps(jsoncontent, indent=4))


