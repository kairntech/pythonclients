import requests
import json

headers = {"Accept": "application/json",
           "Content-Type": "application/json"}


def call_sherpa(text,model,annotator,server,token,chunklength=10000000):
    url = server + "/projects/" + model + "/annotators/" + annotator + "/_annotate"
    results = {}
    text = text.encode(encoding='utf-8')
    #print("calling sherpa server '%s' ... " % url)
    headers = {"Accept": "application/json",
               "Content-Type": "text/plain",
               "Authorization": "Bearer " + token}

    # HACK! some annotators (delft ...) have issues with long texts, so cut them
    #print("length(text) = %s" % len(text))
    texts = [text[i:i+chunklength] for i in range(0,len(text),chunklength)]
    for chunk in texts:
        print(".", end='')
        response = requests.post(url,data=chunk, headers=headers)
        #print("response = %s" % response.content)
        if (response.status_code != 200):
            print("error from server: %s" % response.status_code)
            continue
        json_response = json.loads(response.text)        
        documenttext = json_response['text']
        annotations = json_response['annotations']
        for annotation in annotations:
            start   = annotation['start']
            end     = annotation['end']
            term    = documenttext[start:end]
            type    = annotation['labelName']
            if type in results:
                results[type].extend([term])
            else:
                results[type] = [term]
    #print("before results = %s" % results)
    for key in results.keys():
        list_set = set(results[key])
        results[key] = list(list_set)
    #print("after results = %s" % results)
    return results
    
def get_token(server, login_info):
    url = server + "/auth/login"
    #print("calling sherpa server '%s' ..." % url)
    try:
        response = requests.post(url,data=login_info, headers=headers)
        json_response = json.loads(response.text)
    except Exception as ex:
        print("Error connecting to Sherpa server %s: %s" % (server, ex))
        return 
    #print("response = %s" % response.text)
    if 'access_token' in json_response:
        token = json_response['access_token']
        return token
    else:
        return 


def get_projects(server,token):
    url = server + "/projects"
    headers2 = {'Authorization': 'Bearer ' + token}
    #print("calling sherpa server '%s' ..." % url)
    response = requests.get(url,headers=headers2)
    json_response = json.loads(response.text)
    projects = ", ".join([project['name'] for project in json_response])
    print("Available projects on %s: %s" % (server, projects))

def get_annotators(server,project,token):
    url = server + "/projects/%s/annotators_by_type" % project
    headers2 = {'Authorization': 'Bearer ' + token}
    #print("calling sherpa server '%s' ..." % url)
    response = requests.get(url,headers=headers2)
    json_response = json.loads(response.text)
    annotators = ['sherpa'] # that is the default (suggester)
    if json_response['learner'] is not None:
        learners = [learner['name'] for learner in json_response['learner']]
        annotators.extend(learners)
    astring = ", ".join(annotators)
    print("Available annotators for project %s on server %s: %s" % (project, server, astring))
    

