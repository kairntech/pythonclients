

Stefan Geißler, July 16 2020

Experiment: Analyse a txt categorization corpus (txt files for each
category sit in a subdirectory with the name of the resp. category)

Then the script performs:

- a comparison of each category with each other, returning the
"similarity" (how close the tdifd ranked feature vectors are)

- the identification of possible outliers: Compare each document's
vector with the vector of its class. Assumption: Perhaps the ones that
rank low are not well placed in their category

Have applied that to Boehringer data and found quite a number of
questionable cases. Perhaps havig such a functionality would allow
Sherpa users to quickly identify problem in the corpus ...

Usage:
- prepare Text corpus

MYCORPUS
 CATEGORY-A
  File1.txt
  File2.txt
 CATEGORY-B
  File3.txt
  File4.txt
 ...

- Then call

python3 getoutliers.py MYCORPUS > result

and inspect the results where first the cat/cat similarities are printed, then the document / cat similarities

Enjoy!
